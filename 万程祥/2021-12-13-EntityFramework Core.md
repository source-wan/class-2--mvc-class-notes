# 20211213-EntityFramework Core

## 介绍

```
EF Core 可用作对象关系映射程序 (O/RM)，这可以实现以下两点：
使 .NET 开发人员能够使用 .NET 对象处理数据库。
无需再像通常那样编写大部分数据访问代码。

```
```C#
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Intro
{

    public class Library
    {
        public int Id { get; set; }
        public string LibraryName { get; set; }
        public string Master { get; set; }
        public List<book> Book { get; set; }
    }

    public class Book
    {
        public int BookId { get; set; }
        public string ISBN { get; set; }
        public string Writer { get; set; }

        public int LibraryId { get; set; }
        public Library Library { get; set; }
    }
}

```

## 命令
```
创建数据库上下文，配置数据库连接字符
dotnet add package Microsoft.EntityFrameworkCore -v 3.1

生成数据库表代码
dotnet ef migrations add InitialCreate

生成的迁移文件，与数据库表信息同步
dotnet ef databasa update
```