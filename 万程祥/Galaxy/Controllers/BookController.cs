using System;
using System.Collections.Generic;

using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Galaxy.Request;
using Galaxy.Models;
using Galaxy.Repository;

namespace Galaxy.Controllers
{
    public class BookController:Controller
    {
    public IActionResult Index()
    {
        // var _db1 =new EfRepository<BookLibrary>();
        // var bl = new BookLibrary{
        //         Name="太陽",
        //         Master="張三",
        //         Address="民大"

        // };
        // _db1.Insert(bl);

        var _db =new EfRepository<Book>();
        // var bk = new Book{
                
        //         Name="活著",
        //         Author="張三",
        //         Price=12,
        //         BookLibraryId=1,
        //         ISBN="AS56DF45"
            
        // };

        // _db.Insert(bk);

       var date= _db.table.Where(x=>x.IsDeleted==false).ToList();
        return View(date);
    }
        public IActionResult CreateOrEdit(int Id)
        {
            var _db =new EfRepository<Book>();
            var date= _db.table.Where(x=>x.Id==Id).FirstOrDefault();
            return View(date);
        }

        public IActionResult save(BookPamra req)
        {
            var _db =new EfRepository<Book>();
            if(req.Id==0)
            {
                var bk = new Book{
                    Name=req.Name,
                    Author=req.Author,
                    Price=req.Price,
                    BookLibraryId=req.BookLibraryId,
                    ISBN=req.ISBN
                };
                _db.Insert(bk);
            }else{
               var bk = _db.table.FirstOrDefault(x=>x.Id==req.Id);
                    bk.Name=req.Name;
                    bk.Author=req.Author;
                    bk.Price=req.Price;
                    bk.BookLibraryId=req.BookLibraryId;
                    bk.ISBN=req.ISBN;
                _db.Update(bk);
            }
            return Ok();
        }

        public IActionResult Query(BookPamra req)
        {
            var _db =new EfRepository<Book>();

            if (req.Name!=null)
            {
                var bk = _db.table.Where(x=>x.IsDeleted==false && (x.Id==req.Id||x.Name.Contains(req.Name)||x.ISBN.Contains(req.ISBN)||x.Price==req.Price||x.BookLibraryId==req.BookLibraryId ||x.Author.Contains(req.Author)));
                return Json(bk.ToList());
            }else{
                var bk= _db.table.Where(x=>x.IsDeleted==false);
                return Json(bk.ToList());
            }
            
        
            
            
        }
        public IActionResult Delete(int Id)
        {
            var _db =new EfRepository<Book>();
            
            _db.Delete(Id);
            return Ok();
        }


    }
}