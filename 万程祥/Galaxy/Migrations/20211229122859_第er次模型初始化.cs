﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Galaxy.Migrations
{
    public partial class 第er次模型初始化 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "master",
                table: "BookLibrary",
                newName: "Master");

            migrationBuilder.RenameColumn(
                name: "address",
                table: "BookLibrary",
                newName: "Address");

            migrationBuilder.RenameColumn(
                name: "price",
                table: "Book",
                newName: "Price");

            migrationBuilder.RenameColumn(
                name: "authorv",
                table: "Book",
                newName: "Authorv");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Master",
                table: "BookLibrary",
                newName: "master");

            migrationBuilder.RenameColumn(
                name: "Address",
                table: "BookLibrary",
                newName: "address");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "Book",
                newName: "price");

            migrationBuilder.RenameColumn(
                name: "Authorv",
                table: "Book",
                newName: "authorv");
        }
    }
}
