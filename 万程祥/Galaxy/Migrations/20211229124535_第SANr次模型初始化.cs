﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Galaxy.Migrations
{
    public partial class 第SANr次模型初始化 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Authorv",
                table: "Book");

            migrationBuilder.AddColumn<string>(
                name: "Author",
                table: "Book",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Author",
                table: "Book");

            migrationBuilder.AddColumn<string>(
                name: "Authorv",
                table: "Book",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
