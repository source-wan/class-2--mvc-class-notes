using System;
namespace Galaxy.Models
{
    public class BaseEntity
    {
        public int Id{set;get;}
        public bool IsDeleted{set;get;}
        public bool IsActived{set;get;}
        public DateTime CreateTime{set;get;}
        public DateTime UpdateTime{set;get;}
        public int DisplayOrder{set;get;}
  
    }
}