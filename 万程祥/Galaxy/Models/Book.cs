
namespace Galaxy.Models
{
    public class Book:BaseEntity
    {
        public string Name{set;get;}
        public string Author{set;get;}
        public int Price{set;get;}
        public string ISBN{set;get;}
        public int BookLibraryId{set;get;}
        public BookLibrary BookLibrary{set;get;}
    }
}