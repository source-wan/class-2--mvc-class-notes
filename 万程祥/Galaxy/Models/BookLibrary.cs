using System.Collections.Generic;
namespace Galaxy.Models
{
    public class BookLibrary:BaseEntity
    {
        public string Name{set;get;}
        public string Master{set;get;}
        public string Address{set;get;}
        public IEnumerable<Book> Book{set;get;}
    }
}