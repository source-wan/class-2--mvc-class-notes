using Galaxy.Models;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Galaxy.Db;
namespace Galaxy.Repository
{
    public class EfRepository<T> :IRepository<T> where T :BaseEntity
    {
        private readonly Dbdemo _db;
       private readonly DbSet<T> _table;
    public IQueryable<T> table
    {
        get{
            return _table;
        }
    }
    public EfRepository()
    {
        _db=new Dbdemo();
        _table=_db.Set<T>(); 
    }
    public void Insert(T entity){
        entity.CreateTime=DateTime.Now;
        entity.DisplayOrder=0;
        entity.UpdateTime=DateTime.Now;
        entity.IsDeleted=false;
        entity.IsActived=true;

        _table.Add(entity);
        _db.SaveChanges();
    }
    public void Delete(int Id){
        var entity= _table.FirstOrDefault(x=>x.Id==Id);
        entity.IsDeleted=true;
        _table.Update(entity);
        _db.SaveChanges();
    }

    public void Update(T entity){
        entity.UpdateTime=DateTime.Now;
        _table.Update(entity);
        _db.SaveChanges();
    }
        
    }
}