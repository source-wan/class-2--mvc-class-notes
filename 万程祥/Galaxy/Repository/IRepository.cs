
namespace Galaxy.Repository
{
    public interface IRepository<T>
    {
        void Insert(T entity);
        void Delete(int Id);
        void Update(T entity);
    }
}