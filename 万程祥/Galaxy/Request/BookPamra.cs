
namespace Galaxy.Request
{
    public class BookPamra
    {
        public int Id{set;get;}
        public string Name{set;get;}
        public string Author{set;get;}
        public int Price{set;get;}
        public string ISBN{set;get;}
        public int BookLibraryId{set;get;}

    }
}