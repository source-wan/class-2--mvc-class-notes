$(function(){
    $('#btnAdd').click(function(){
        window.location.href='/Book/CreateOrEdit'
    })
    $('#btnQuery').click(function(){
        var Id=$('[name=keyword]').val();
        var Name=$('[name=keyword]').val();
        var Price=$('[name=keyword]').val();
        var isbn=$('[name=keyword]').val();
        var BookLibraryId=$('[name=keyword]').val();
        var Author=$('[name=keyword]').val();
        
        var obj={
            Id,
            Name,
            Price,
            isbn,
            BookLibraryId,
            Author
        }
        $.post('/book/Query',obj,function(res){

            $('[name=content]').html('');
            res.forEach(item => {
                let html = 
                `
                <tr name="content">
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>${item.author}</td>
                    <td>${item.isbn}</td>
                    <td>${item.bookLibraryId}</td>
                    <td>
                        <input type="button" value="編輯" onclick="btnEdit(${item.Id})">
                        <input type="button" value="刪除" onclick="btnDelete(${item.Id})">
                    </td>
                </tr>
                `
                $('#top').append(html);
            });

        })

    })

    $('#btncancel').click(function(){
        window.location.href='/Book/index'
    })

    $('#btnsave').click(function(){
        var Id=$('[name=Id]').val();
        var Name=$('[name=Name]').val();
        var Price=$('[name=Price]').val();
        var ISBN=$('[name=ISBN]').val();
        var BookLibraryId=$('[name=BookLibraryId]').val();
        var Author=$('[name=Author]').val();
        
        var obj={
            Id,
            Name,
            Price,
            ISBN,
            BookLibraryId,
            Author
        }
        $.post('/book/save',obj,function(){
            window.location.href='/Book/index'
        })
    
    })

})

function btnEdit(Id)
{
    window.location.href=`/Book/CreateOrEdit/${Id}`
}
function btnDelete(Id)
{
    var obj={
        Id
    }
    $.post('/book/Delete',obj,function(){
        window.location.href='/Book/index'
    })


}